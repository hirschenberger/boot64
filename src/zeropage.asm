; vim: ft=64tass

zp .proc
    


;   +----------+---------------------------------------------------+
;   | Bits 7-6 |   Undefined                                       |
;   | Bit  5   |   Cassette Motor Control (0 = On, 1 = Off)        |
;   | Bit  4   |   Cassette Switch Sense: 1 = Switch Closed        |
;   | Bit  3   |   Cassette Data Output Line                       |
;   | Bit  2   |   /CharEn-Signal (see Memory Configuration)       |
;   | Bit  1   |   /HiRam-Signal  (see Memory Configuration)       |
;   | Bit  0   |   /LoRam-Signal  (see Memory Configuration)       |
;   +----------+---------------------------------------------------+
;   Default Value is $37/55 (%00110111).
    pla_io_control = $01

    .virtual $0092
        w0 .word ?  
        w1 .word ?
        w2 .word ?
    .endv
    .virtual $00a3
        w3 .word ?
        w4 .word ?
        w5 .word ?
    .endv

    .virtual $00a8
        b0 .byte ?
        b1 .byte ?
        b2 .byte ?
        b3 .byte ?
        b4 .byte ?
        b5 .byte ?
        b6 .byte ?
    .endv
    
    .virtual $0314
        irq_vec .addr ?
        irq_brk .addr ?
        irq_nmi .addr ?
    .endv
.pend
