; vim: ft=64tass

BasicStart .macro label
*= $0801
    .word (+), 2005 ;pointer, line number
    .null $9e, format("%d", \label)  ;will be sys 4096
+   .word 0                          ;basic line end
.endm

InitScreen .macro background, border
    ldx #\background
    stx vic.cbg
    ldx #\border
    stx vic.cborder
.endm

ScreenPos .function xx, yy
.endf $0400 + yy*40 + xx


DrawText .macro xx, yy, text
        ldx #$00
    _loop
        lda \text,x
        beq _end
        sta ScreenPos(\xx, \yy),x
        inx
        jmp _loop
    _end
.endm

SetBank .macro bank
    bank_num = 3 - \bank
    lda #bank_num
    sta cia2.data_port_a
.endm

Copy1kb .macro from, to
        ldx #0
    -   .for offs in [$0, $100, $200, $300]
            lda \from+offs,x
            sta \to+offs,x
        .next
        inx
        bne -
.endm

Memset1kb .macro to, value
        ldx #0
    -   .for offs in [$0, $100, $200, $300]
            lda #\value
            sta \to+offs,x
        .next
        inx
        bne -
.endm

SetScreenColor .macro xx, yy, color
    lda \color
    sta $0400 + \yy*40 + \xx
.endm

StoreAddr .macro addr, to
    lda #<\addr
    sta \to
    lda #>\addr
    sta \to+1
.endm

MirrorCharV .macro charset, from, to
        .cerror \from ==\to, "Character regions may not be the same"
        ldx #7
        ldy #0
    -
        lda \charset + \from*8,x
        sta \charset + \to*8,y
        iny
        dex
        bpl -
.endm

MirrorCharH .macro charset, from, to
        .cerror \from == \to, "Character regions may not be the same"
        ldx #7
    -
        ldy #7
        lda \charset + \from*8,x
    -   
        rol a                    
        ror \charset + \to*8,x      
        dey
        bpl -
        dex
        bpl --
.endm

PushRegs .macro
    pha
    txa
    pha
    tya
    pha
.endm

PopRegs .macro
    pla
    tay
    pla
    tax
    pla
.endm

Break .function bplocation
    .section monitor_commands ; write breakpoint command
    .text format("bk %x", bplocation), 10
    .send
.endf


.section code
; wait x register times vblank (16ms)
wait_x16ms
    _waitvb
        bit $D011
        bpl _waitvb
    _waitvb2:
        bit $D011
        bmi _waitvb2

        dex
        bne _waitvb
        rts
.send
