; vim: ft=64tass

CIA_FREQ_HZ = 985248

CIA_TIMER_TICKS_MS = CIA_FREQ_HZ/1000


cia1 .proc
;   +----------+---------------------------------------------------+
;   | Bits 7-0 |   Write Keyboard Column Values for Keyboard Scan  |
;   | Bits 7-6 |   Paddles on: 01 = Port A, 10 = Port B            |
;   | Bit  4   |   Joystick A Fire Button: 0 = Pressed             |
;   | Bit  3   |   Joystick A Right: 0 = Pressed, or Paddle Button |
;   | Bit  2   |   Joystick A Left : 0 = Pressed, or Paddle Button |
;   | Bit  1   |   Joystick A Down : 0 = Pressed                   |
;   | Bit  0   |   Joystick A Up   : 0 = Pressed                   |
;   +----------+---------------------------------------------------+
    dataport_a = $dc00

;   +----------+---------------------------------------------------+
;   | Bits 7-0 |   Read Keyboard Row Values for Keyboard Scan      |
;   | Bit  4   |   Joystick B Fire Button: 0 = Pressed             |
;   | Bit  3   |   Joystick B Right: 0 = Pressed, or Paddle Button |
;   | Bit  2   |   Joystick B Left : 0 = Pressed, or Paddle Button |
;   | Bit  1   |   Joystick B Down : 0 = Pressed                   |
;   | Bit  0   |   Joystick B Up   : 0 = Pressed                   |
;   +----------+---------------------------------------------------+
    dataport_b = $dc01

    timer_a_lo = $dc04 ; Timer A Low-Byte  (Kernal-IRQ, Tape)
    timer_a_hi = $dc05 ; Timer A High-Byte  (Kernal-IRQ, Tape)
    timer_b_lo = $dc06 ; Timer B Low-Byte  (Tape, Serial Port)
    timer_b_hi = $dc07 ; Timer B High-Byte (Tape, Serial Port)
;   +-------+------------------------------------------------------+
;   | Bit 7 |   On Read:  1 = Interrupt occured                    |
;   |       |   On Write: 1 = Set Int.-Flags, 0 = Clear Int-.Flags |
;   | Bit 4 |   FLAG1 IRQ (Cassette Read / Serial Bus SRQ Input)   |
;   | Bit 3 |   Serial Port Interrupt ($DC0C full/empty)           |
;   | Bit 2 |   Time-of-Day Clock Alarm Interrupt                  |
;   | Bit 1 |   Timer B Interrupt (Tape, Serial Port)              |
;   | Bit 0 |   Timer A Interrupt (Kernal-IRQ, Tape)               |
;   +-------+------------------------------------------------------+
    irq = $dc0d

;   +-------+--------------------------------------------------------+
;   | Bit 7 |   Time-of-Day Clock Frequency: 1 = 50 Hz, 0 = 60 Hz    |
;   | Bit 6 |   Serial Port ($DC0C) I/O Mode: 1 = Output, 0 = Input  |
;   | Bit 5 |   Timer A Counts: 1 = CNT Signals, 0 = System 02 Clock |
;   | Bit 4 |   Force Load Timer A: 1 = Yes                          |
;   | Bit 3 |   Timer A Run Mode: 1 = One-Shot, 0 = Continuous       |
;   | Bit 2 |   Timer A Output Mode to PB6: 1 = Toggle, 0 = Pulse    |
;   | Bit 1 |   Timer A Output on PB6: 1 = Yes, 0 = No               |
;   | Bit 0 |   Start/Stop Timer A: 1 = Start, 0 = Stop              |
;   +-------+--------------------------------------------------------+
    timer_a_ctrl = $dc03

;   +----------+-----------------------------------------------------+
;   | Bit  7   |   Set Alarm/TOD-Clock: 1 = Alarm, 0 = Clock         |
;   | Bits 6-5 |   Timer B Mode Select:                              |
;   |          |            00 = Count System 02 Clock Pulses        |
;   |          |            01 = Count Positive CNT Transitions      |
;   |          |            10 = Count Timer A Underflow Pulses      |
;   |          |            11 = Count Timer A Underflows While CNT  |
;   | Bit  4   |   Force Load Timer B: 1 = Yes                       |
;   | Bit  3   |   Timer B Run Mode: 1 = One-Shot, 0 = Continuous    |
;   | Bit  2   |   Timer B Output Mode to PB7: 1 = Toggle, 0 = Pulse |
;   | Bit  1   |   Timer B Output on PB7: 1 = Yes, 0 = No            |
;   | Bit  0   |   Start/Stop Timer B: 1 = Start, 0 = Stop           |
;   +----------+-----------------------------------------------------+
    timer_b_ctrl = $dc0f
.pend


cia2 .proc
;   +----------+---------------------------------------------------+
;   | Bit  7   |  Serial Bus Data Input                            |
;   | Bit  6   |  Serial Bus Clock Pulse Input                     |
;   | Bit  5   |  Serial Bus Data Output                           |
;   | Bit  4   |  Serial Bus Clock Pulse Output                    |
;   | Bit  3   |  Serial Bus ATN Signal Output                     |
;   | Bit  2   |  RS232 Data Output (User Port)                    |
;   | Bit  1-0 |  VIC Chip System Memory Bank Select (low active!) |
;   +----------+---------------------------------------------------+
    data_port_a = $dd00 
.pend
