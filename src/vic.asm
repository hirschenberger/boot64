; vim: ft=64tass

vic .proc
    color .block
        BLACK  = $0
        WHITE  = $1
        RED    = $2
        CYAN   = $3
        PURPLE = $4
        GREEN  = $5
        BLUE   = $6
        YELLOW = $7
        ORANGE = $8
        BROWN  = $9
        LRED   = $a
        GRAY1  = $b
        GRAY2  = $c
        LGREEN = $d
        LBLUE  = $e
        GRAY3  = $f
    .bend

    sprite .block
        xs0      = $d000
        ys0      = $d001
        xs1      = $d002
        ys1      = $d003
        xs2      = $d004
        ys2      = $d005
        xs3      = $d006
        ys3      = $d006
        xs4      = $d008
        ys4      = $d009
        xs5      = $d00a
        ys5      = $d00b
        xs6      = $d00c
        ys6      = $d00d
        xs7      = $d00e
        ys7      = $d00f
        msb_xs   = $d010      ; bit 8 of x position   (one bit per sprite)
    .bend

    base = $d000
    controlv    = base + $11    ; vertical control and other stuff:
    ; %7....... "bit 8" of $d012
    ; %.6...... extended background color mode (1=on)
    ; %..5..... 0 = text mode, 1 = bitmap mode
    ; %...4.... display enable: 0 = disable (border only), 1 = enable   VIC only checks this once per frame!
    ; %....3... lines: 0 = 24, 1 = 25   ; if set, upper and lower border gain 4 pixels each
    ; %.....210 vertical smooth scroll (default %011), screen contents are moved down by this amount
    line     = base + $12    ; raster line (also see bit 7 of $d011) reading returns current value, writing sets interrupt value
    xlp      = base + $13    ; light pen coordinates, x (only half the resolution)
    ylp      = base + $14    ; light pen coordinates, y
    sactive  = base + $15    ; sprite enable (one bit per sprite)
    controlh = base + $16    ; horizontal control and other stuff:
    ; %76...... always set
    ; %..5..... "test", should be 0
    ; %...4.... 0 = hires, 1 = multicolor
    ; %....3... columns: 0 = 38, 1 = 40 ; if set, left border gains 7 pixels and right border gains 9 pixels
    ; %.....210 horizontal smooth scroll (default %000), screen contents are moved to the right by this amount
    sdy     = base + $17    ; sprites, double height    (one bit per sprite)
    ram     = base + $18    ; RAM pointer
    ; %7654.... which K of VIC bank is video ram (default %0001)
    ; %....321. text: which 2K of VIC bank is character set (default %010)
    ; %.......0 text: unused
    ; %....3... bitmap: which 8K of VIC bank is bitmap
    ; %.....210 bitmap: unused
    irq     = base + $19    ; writing back acknowledges!
    ; %7....... 1: VIC requested interrupt
    ; %.654.... always set
    ; %....3... 1: light pen active
    ; %.....2.. 1: sprite-sprite collision
    ; %......1. 1: sprite-data collision
    ; %.......0 1: raster interrupt
    irqmask = base + $1a
    ; %7654.... always set
    ; %....3... 1: enable lightpen interrupt
    ; %.....2.. 1: enable sprite-sprite collision interrupt
    ; %......1. 1: enable sprite-data collision interrupt
    ; %.......0 1: enable raster interrupt
    sback       = base + $1b    ; sprites, background   (one bit per sprite)
    smc         = base + $1c    ; sprites, multicolor   (one bit per sprite)
    sdx         = base + $1d    ; sprites, double width (one bit per sprite)
    ss_collided = base + $1e    ; sprites, sprite collision (one bit per sprite)    reading clears register!
    sd_collided = base + $1f    ; sprites, data collision   (one bit per sprite)    reading clears register!
    ; color registers (high nibbles are always %1111):
    cborder = base + $20    ; border color
    cbg     = base + $21    ; general background color
    cbg0    = base + $21
    cbg1    = base + $22    ; background color 1 (for EBC and MC text mode)
    cbg2    = base + $23    ; background color 2 (for EBC and MC text mode)
    cbg3    = base + $24    ; background color 3 (for EBC mode)
    sc01    = base + $25    ; sprite color for MC-bitpattern %01
    sc11    = base + $26    ; sprite color for MC-bitpattern %11
    ; individual sprite colors (in MC mode, these are used for bit pattern %10):
    cs0     = base + $27    ; sprite 0 color
    cs1     = base + $28    ; sprite 1 color
    cs2     = base + $29    ; sprite 2 color
    cs3     = base + $2a    ; sprite 3 color
    cs4     = base + $2b    ; sprite 4 color
    cs5     = base + $2c    ; sprite 5 color
    cs6     = base + $2d    ; sprite 6 color
    cs7     = base + $2e    ; sprite 7 color
.pend
