; vim: ft=64tass

; Change the current game stage by just providing the scopename where the callbacks are.
; Required callbacks are:
;   - init
;   - draw
;   - update
;   - timer
; NOTE: disable IRQs when setting the new stage
SetCurrentStage .macro name
    php
    sei
    ; set addresses
    #StoreAddr \name.update, game_loop.jt.update
    #StoreAddr \name.draw, game_loop.jt.draw
    #StoreAddr \name.timer, game_loop.jt.timer
    ; reset status flags
    lda #%1000_0000
    sta game_loop.status
    ; and initialize the stage
    jsr \name.init
    cli
    plp
.endm

game_loop .proc
    ;--------------------------------------------------------------------------
    ; User changeable defines

    IRQ_RASTERLINE  :?= $0000   ; the rasterline where the draw callback is emitted
    UPDATE_FREQ_MS  :?= 50      ; the update interval for the update callback
    TIMER_UPDATES   :?= 5      ; the number of updates for a timer callback 

    ; sanity checks
    .cerror IRQ_RASTERLINE > $138, "The maximum allowed rasterline is: ", $138 
    .cerror UPDATE_FREQ_MS * CIA_TIMER_TICKS_MS > 1<<16, format(
            "IRQ timer freq too high. Max value: %d", 
            (1<<16)/CIA_TIMER_TICKS_MS) 

    ;-------------------------------------------------------------------------
    ; the jumptable to the current functions in the active gamestage
    jt .proc
        .virtual $57 ; - $5c
            update      .word ?
            draw        .word ?
            timer       .word ?
        .endv
    .pend

    .virtual $5d
        ;-----------------------------------------------------------------------------------------
        ; Statusflags for the gameloop
        ; R---_----
        ;           R: Is the gameloop running. 1: running, 0: finished
        ;              The other flags may be used by states to notify about specific information
        status .byte ?
    .endv

    
    .section code
    start
        sei
        ; enable raster irq 
        lda vic.irqmask
        ora #1           
        sta vic.irqmask
        
        lda #<IRQ_RASTERLINE
        sta vic.line
        ; set highbit of rasterline in vic.controlv
        lda #>IRQ_RASTERLINE
        ror a
        lda #0
        ror a
        sta zp.b0
        lda vic.controlv
        and #%0111_1111
        ora zp.b0
        sta vic.controlv
        

        ; setup timer a
        lda #<UPDATE_FREQ_MS*CIA_TIMER_TICKS_MS
        sta cia1.timer_a_lo
        lda #>UPDATE_FREQ_MS*CIA_TIMER_TICKS_MS
        sta cia1.timer_a_hi
        
        lda #%0001_0001
        sta cia1.timer_a_ctrl

        ; setup timer b
        lda #<TIMER_UPDATES
        sta cia1.timer_b_lo
        lda #>TIMER_UPDATES
        sta cia1.timer_b_hi

        lda #%0101_0001
        sta cia1.timer_b_ctrl

        ; set irq vector
        #StoreAddr irq_switch, zp.irq_vec

        #SetCurrentStage empty_stage
        cli
        rts

    irq_switch
        lda vic.irq
        bpl +
        jsr jump_draw
        dec vic.irq         ; confirm irq
    +
        lda cia1.irq
        pha
        and #%1000_0001     ; check for timer A source
        cmp #%1000_0001
        bne +
        jsr jump_update
    +   
        pla
        and #%1000_0010     ; check for timer B source
        cmp #%1000_0010
        bne +
        jsr jump_timer
    +
        jmp kernal.irq_end 


    ; Trampoline functions ----------------------------------------------------------------
    jump_update
        jmp (jt.update)

    jump_draw
        jmp (jt.draw)

    jump_timer
        jmp (jt.timer)

    ; Empty dummy stage at init ----------------------------------------------------------
    empty_stage .proc
        init
            #SetBank 0
            lda #%0000_1000         
            sta vic.controlv
            lda #%0000_1000         
            sta vic.controlh
            lda #%0001_1000         
            sta vic.ram
            lda #vic.color.BLUE    
            sta vic.cborder
            sta vic.cbg
            rts
        draw
            rts
        update
            rts
        timer
            rts
    .pend
.send
.pend




; -------------------------------------------------------------------------------------    
; Demo stage, with a set of spinners to visualize the callback updates
demo_stage .proc
    .section code
    ; -------------------------------------------------------------------------------------    
    ; Default callback functions, with a set of spinners to visualize the callback updates
    init
        jsr kernal.clear_screen

        ldy zp.pla_io_control    ; save old value
        lda #$33
        sta zp.pla_io_control    ; blend in char rom 
        #Copy1kb $d000, $3000    ; copy chars from rom to ram in same area
        #Copy1kb $d400, $3400
        sty zp.pla_io_control    ; restore old value

        lda vic.ram
        ora #%0000_1100
        and #%1111_1101
        sta vic.ram

        ; we have no right and down arrow, so we mirror the existing ones
        #MirrorCharV $3000, $1e, $1c
        #MirrorCharH $3000, $1f, $1d
        ; also the inverted ones
        #MirrorCharV $3000, $9e, $9c
        #MirrorCharH $3000, $9f, $9d
        
        #DrawText 9, 2, title_txt
        #DrawText 0, 4, draw_txt
        #DrawText 0, 5, update_txt
        #DrawText 0, 6, timer_txt
        #DrawText 0, 7, joy_a_txt
        #DrawText 0, 8, joy_b_txt

        ; init spinners
        lda #0
        sta zp.b0
        sta zp.b1
        sta zp.b2
    
        rts

    update
        
        ldx zp.b0
        lda spinner,x
        bne +
        ldx #-1
        lda spinner
    +
        sta ScreenPos(14, 5)
        inx
        stx zp.b0
        rts

    draw
        #DrawJoystick cia1.dataport_a, ScreenPos(14, 7)
        #DrawJoystick cia1.dataport_b, ScreenPos(14, 8)
        ldx zp.b1
        lda spinner,x
        bne +
        ldx #0
        lda spinner
    +
        sta ScreenPos(14, 4)
        inx
        stx zp.b1
        rts
    
    timer
        ldx zp.b2
        lda spinner,x
        bne +
        ldx #0
        lda spinner
    +
        sta ScreenPos(14, 6)
        inx
        stx zp.b2
        rts

DrawJoystick .macro port, pos
        symbols = [$1e, $1c, $1f, $1d]
        ;#Break *

        .bfor i in range(len(symbols))
            lda #1<<i
            bit \port
            bne _skip
            lda #symbols[i]
            sta \pos
        _skip
        .next
        
        lda #%0000_1111
        and \port
        cmp #%0000_1111
        bne _dir
        lda #$57
        sta \pos
    _dir
        
        ; fire pressed?
        lda #%0001_0000
        bit \port
        bne _no_fire
        lda \pos
        clc
        adc #$80
        sta \pos
    _no_fire
.endm

    .enc "screen"
        title_txt   .null "●●●● gameloop demo ●●●●"
        update_txt  .null "update loop: "
        draw_txt    .null "draw loop:   "
        timer_txt   .null "timer loop:  "
        joy_a_txt   .null "joystick a:  "
        joy_b_txt   .null "joystick b:  "
    .enc "none"
    spinner .null $4d, $40, $4e, $5d
    .send
.pend

