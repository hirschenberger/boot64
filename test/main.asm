; vim: ft=64tass

.include "macros.asm"
.include "vic.asm"
.include "cia.asm"
.include "kernal.asm"
.include "zeropage.asm"
.include "game_loop.asm"

TEST :?= false
.if TEST
    .include "../deps/c64unit/cross-assemblers/64tass/core2000.asm"
.else



; Start ----------------------------------------------------------------------------------- 
#BasicStart start

.section code
    start
            jsr game_loop.start
            sei
            #SetCurrentStage demo_stage
            cli

        -   jmp -
.send

.fi

; Memory layout -------------------------------------------------------------------------- 
* = $1000
.dsection code

.virtual 0  ; virtual section for command file
    .dsection monitor_commands
.endv

; Unittests --------------------------------------------------------------------------------
.if TEST
c64unit

    examineTest testVicColors
    examineTest testScreencolor 
    examineTest testStoreAddr
    examineTest testCopyAddrZp
;    examineTest testPushPopRegs
    examineTest testMirrorChar
c64unitExit


testVicColors .proc
    lda #vic.color.BLACK
    assertEqualToA #$00, "black-not-black"
    rts
.pend

testCopyAddrZp .proc
    src = $dead
    #StoreAddr src, zp.w0
    assertWordEqual src, zp.w0
    rts
.pend

testStoreAddr .proc
    ; set src to $dead
    src = $dead
    dest = $3000

    #StoreAddr src, dest
    assertWordEqual src, dest
    rts
.pend

testPushPopRegs .proc
    ldy #3
    ldx #2
    lda #1

    #PushRegs
    ldy #0
    ldx #0
    lda #0
    #PopRegs

    assertEqualToA #1
    assertEqualToX #2
    assertEqualToY #3
    rts
.pend

testMirrorChar .proc
        #MirrorCharH orig, $00, $01 
        #MirrorCharH orig, $01, $02 
        assertMemoryEqual orig, back, $08
        rts

    orig
        .for i in range(8) 
            .byte $01<<i
        .next
    dest
        .fill 8, $00
    back
        .fill 8, $00

.pend

testScreencolor .proc
    lda $d011
    ora %0000_1000
    sta $d011
    
    lda $d016
    ora %0000_1000
    sta $d016

    .for i in range(5)
    lastPixel := $400 + i
    assertNotEqual #vic.color.RED, lastPixel
    #SetScreenColor i, 0, #vic.color.RED
    assertEqual #vic.color.RED, lastPixel
    .next
    .for i in range(5)
    lastPixel := $400 + i + 10*40
    assertNotEqual #vic.color.RED, lastPixel
    #SetScreenColor i, 10, #vic.color.RED
    assertEqual #vic.color.RED, lastPixel
    .next
    rts
.pend
.fi
